# IOS App Take Home Challenge
Street Capital is creating an IOS app for busy business folks to track their ToDoList. 
Here are the list of missing features.

## Missing Features

- Add a way for users to update their current list of ToDo items.
- Add Firebase/Parse/Your Own Backend database system to allow users save their current TodoList to the cloud
- Upload your code to GitHub or BitBucket


## Nice To Have

- Add a simple test framework 
- Add the ability for users to sync their CRUD (Create, Read, Update, Delete) operations to the online cloud database system you are using


